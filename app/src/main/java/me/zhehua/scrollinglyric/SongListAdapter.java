
package me.zhehua.scrollinglyric;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.MyViewHolder> {

    private ArrayList<HashMap<String, String>> songList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView songName, singerName;
        public ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            songName = (TextView) view.findViewById(R.id.songNametxt);
            singerName = (TextView) view.findViewById(R.id.singerNametxt);
            icon = (ImageView) view.findViewById(R.id.icon);
        }
    }

    public SongListAdapter(ArrayList<HashMap<String, String>> songList) {
        this.songList = songList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_song_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HashMap<String, String> songHashMap = songList.get(position);
        holder.songName.setText(songHashMap.get("songName"));
        holder.singerName.setText(songHashMap.get("singerName"));
       /* holder.icon.setImageResource(
            .getResources().getIdentifier(, "drawable", getPackageName());
        );*/
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }
}