package me.zhehua.scrollinglyric;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class SongList  {

    public SongList () {

    }

    public String getJSONFromAsset(Activity activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open(
                "song_list.json"
            );
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public ArrayList<HashMap<String, String>> parseJsonSongList(String songJson) {
        if(songJson == null || songJson.isEmpty()) {
            return null;
        }
        try {
            JSONArray jsonArr = new JSONArray(songJson);
            ArrayList<HashMap<String, String>> songList = new ArrayList<>();
            HashMap<String, String> songTempList;
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject jsonObject = jsonArr.getJSONObject(i);
                //Add your values in your `ArrayList` as below:
                songTempList = new HashMap<String, String>();
                songTempList.put("id", jsonObject.getString("id"));
                songTempList.put("singerName", jsonObject.getString("singer_name"));
                songTempList.put("songName", jsonObject.getString("song_name"));
                songTempList.put("songImage", jsonObject.getString("song_image"));
                songTempList.put("songUrl", jsonObject.getString("song_url"));
                songTempList.put("songMusicUrl", jsonObject.getString("song_music_url"));
                songTempList.put("songLyricUrl", jsonObject.getString("song_lyric_url"));
                songList.add(songTempList);
            }
            return songList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
