package me.zhehua.scrollinglyric;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class SongListUnitTest extends TestCase {


    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

    }

    public JSONArray constructJsonObjectForTest() throws JSONException {
        JSONArray array = new JSONArray();
        JSONObject songItem1 = new JSONObject();
        songItem1.put("id", "2");
        songItem1.put("singer_name", "Eagles");
        songItem1.put("song_name", "Hotel California");
        songItem1.put("song_image", "images/hotel_california");
        songItem1.put("song_url", "hotel_california_song");
        songItem1.put("song_music_url", "Yesterday");
        songItem1.put("song_name", "hotel_california_minus");
        songItem1.put("song_lyric_url", "hotel_california_text");
        array.put(songItem1);
        JSONObject songItem2 = new JSONObject();
        songItem2.put("id", "1");
        songItem2.put("singer_name", "The Beatles");
        songItem2.put("song_name", "Yesterday");
        songItem2.put("song_image", "images/beatles_yesterday");
        songItem2.put("song_url", "yesterday_song");
        songItem2.put("song_music_url", "Yesterday");
        songItem2.put("song_name", "yesterday_minus");
        songItem2.put("song_lyric_url", "yesterday_text");
        array.put(songItem2);
        return array;
    }


    @Test
    public void testMethod() throws JSONException {
        assertNotNull(new SongList().parseJsonSongList(
            constructJsonObjectForTest().toString()
        ));
    }
}
